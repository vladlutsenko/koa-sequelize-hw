# Lecture 13 homework

## Start server + db

```
docker-compose up
```
Before starting please make sure that .env file was filled up correctly!


## Endpoints
 - get users:  
    >METHOD: GET
    ```
    http://localhost:3000/user
    ```

 - add user:  
    >METHOD: POST  
    BODY: firstName: string, lastName: string, email: string
    ```
    http://localhost:3000/user
    ```

 - update user:  
    >METHOD: POST  
    BODY: firstName: string, lastName: string, email: string
    ```
    http://localhost:3000/user/:id
    ```

 - delete user:  
    >METHOD: DELETE  
    ```
    http://localhost:3000/user/:id
    ```
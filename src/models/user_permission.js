'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_permission extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  User_permission.init({
    user_id: {
      type: DataTypes.INTEGER,
      references: {
        model: "User",
        key: "id",
      }
    },
    permission_id: {
      type: DataTypes.INTEGER,
      references: {
        model: "Permission",
        key: "id",
      }
    },
  }, {
    sequelize,
    modelName: 'User_permission',
  });
  return User_permission;
};
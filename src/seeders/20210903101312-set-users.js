'use strict';
const {Op} = require('sequelize');
const { Sequelize } = require('../models');


const users = [
  {
    firstName: "john",
    lastName: "doe",
    email: "123@ha",
    password: "123",
    createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
    updatedAt: Sequelize.literal('CURRENT_TIMESTAMP')
  },
  {
    firstName: "mark",
    lastName: "lol",
    email: "123@sadf",
    password: "321",
    createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
    updatedAt: Sequelize.literal('CURRENT_TIMESTAMP')
  },
];

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('Users', users, {});
     

  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('Users', {
      firstName: {
         [Op.in]: users.map(it => it.firstName)
       }
     });
  }
};

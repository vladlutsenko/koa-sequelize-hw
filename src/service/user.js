const { sequelize, User } = require("../models");

async function getUsers() {
    try {
        const result = await User.findAll();
        return result;
    } catch (err) {
        throw new Error(err.message);
    } 
}

async function getUserByEmail(email) {
    try {
        const result = await User.findOne({ where: { email: email } });;
        return result;
    } catch (err) {
        throw new Error(err.message);
    } 
}

async function addUser(user) {
    try {
        const result = await User.create({ ...user });
        return result;
    } catch (err) {
        throw new Error(err.message);
    } 
}

async function updateUser(id, user) {
    try {
        const result = await User.update({ ...user }, { where: { id } });
        return result;
    } catch (err) {
        throw new Error(err.message);
    } 
}

async function deleteUser(id) {
    try {
        const result = await User.destroy({ where: { id } });
        return result;
    } catch (err) {
        throw new Error(err.message);
    } 
}

module.exports = {
    getUsers,
    updateUser,
    addUser,
    deleteUser,
    getUserByEmail
};
  
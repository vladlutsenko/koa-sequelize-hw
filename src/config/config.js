require("dotenv").config();

module.exports = {
  development: {
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB,
    seederStorage: "sequelize",
    seederStorageTableName: "sequelize_data",
    host: process.env.POSTGRES_HOST,
    dialect: "postgres",
    define: {
      timestamps: true,
      freezeTableName: true,
      underscoredAll: true,

      createdAt: "created_at",
      updatedAt: "updated_at",
      underscored: true,
    },
  },
};

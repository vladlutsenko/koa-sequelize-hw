const Koa = require("koa");
const bodyParser = require("koa-bodyparser");
require("dotenv").config();

const userRouter = require('./router/user');
const db = require("./models");

const app = new Koa();

app.use(bodyParser());
app.use(userRouter.routes());


const start = async () => {
    try {
        await db.sequelize.authenticate();
        app.listen(process.env.SERVER_PORT, () => {
        console.log("server started");
        });
    } catch (error) {
        console.error(error);
    }
};

start();
  
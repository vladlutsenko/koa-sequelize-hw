const Router = require('koa-router');
const { handleUserGet,  handleUserAdd, handleUserUpdate, handleUserDelete, getSelf, handleLogin} = require('../controller/user');
const { secret} = require('../config/server');
const jwt = require('koa-jwt');

const router = new Router({ prefix: '/user' });

router.get('/self', jwt({
    secret: secret
}), getSelf);

router.post('/login', handleLogin);

router.get('/', handleUserGet);
router.post('/', handleUserAdd);
router.post('/:id', handleUserUpdate);
router.delete('/:id', handleUserDelete);

module.exports = router;
const { getUsers, addUser, updateUser, deleteUser, getUserByEmail } = require('../service/user');
const bcrypt = require('bcrypt');
const jsonwebtoken = require('jsonwebtoken');
const { secret} = require('../config/server');

async function getSelf(ctx) {
  const user= ctx.state.user;
  console.log(user);

  ctx.body=user;
  ctx.status=200;
}

async function handleLogin(ctx, next) {
  const user = ctx.request.body;

  if (!user.email){
    return ctx.throw(403, "no user provided");
  }

  const userObj = (await getUserByEmail(user.email)).toJSON();
  console.log('user',userObj);

  const {email, password, ...userinfo} = userObj;

  if (await bcrypt.compare(user.password, password)) {
    ctx.body = {
      token: jsonwebtoken.sign({
        subject: userinfo,
        data: {
          user_id: userinfo.id
        },
        exp: Math.floor(Date.now()/1000) + (60 * 60)
      }, secret)
    };
    next();
  }

}

async function handleUserGet(ctx) {
  const users = await getUsers();
  ctx.body = users;
  ctx.status = 200;
}

async function handleUserAdd(ctx) {
  const user = ctx.request.body;
  try {
    const userID = await addUser(user);
    ctx.body = userID;
    ctx.status = 200;
  } catch (error) {
    ctx.status = 400;
  }
}

async function handleUserUpdate(ctx) {
  const id = ctx.params.id;
  const user = ctx.request.body;
  try {
    const response = await updateUser(id, user);
    ctx.body = response;
    ctx.status = 200;
  } catch (error) {
    ctx.status = 400;
  }
}

async function handleUserDelete(ctx) {
  const id = ctx.params.id;
  try {
    const response = await deleteUser(id);
    ctx.body = response;
    ctx.status = 200;
  } catch (error) {
    ctx.status = 400;
  }
}


module.exports = {
    handleUserGet,
    handleUserUpdate,
    handleUserDelete,
    handleUserAdd,
    getSelf,
    handleLogin
};